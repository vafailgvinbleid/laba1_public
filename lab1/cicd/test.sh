#!/bin/bash

all_tests_passed=true

echo "2 3 6 n" > temp_input.txt
output=$(../usr/bin/findMax < temp_input.txt)
expected_output=6
if [ "$output" -eq "$expected_output" ]; then
echo "Test case 1 passed"
else
echo "Test case 1 failed: Expected $expected_output but got $output"
all_tests_passed=false
fi

echo "3 -5 -2 g" > temp_input.txt
output=$(../usr/bin/findMax < temp_input.txt)
expected_output=3
if [ "$output" -eq "$expected_output" ]; then
echo "Test case 2 passed"
else
echo "Test case 2 failed: Expected $expected_output but got $output"
all_tests_passed=false
fi

echo "0 0 0 f" > temp_input.txt
output=$(../usr/bin/findMax < temp_input.txt)
expected_output=0
if [ "$output" -eq "$expected_output" ]; then
echo "Test case 3 passed"
else
echo "Test case 3 failed: Expected $expected_output but got $output"
all_tests_passed=false
fi

rm temp_input.txt

if [ "$all_tests_passed" = true ]; then
exit 0 # Success
else
exit 1 # Failure
fi
