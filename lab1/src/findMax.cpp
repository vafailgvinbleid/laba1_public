#include <iostream>
#include <vector>
#include <limits>

// Функция для поиска максимального значения в массиве
int findMax(const std::vector<int>& arr) {
    if (arr.empty()) {
        std::cerr << "Ошибка\n";
        return std::numeric_limits<int>::min(); // Возвращаем минимальное значе>
    }

    int max = arr[0];
    for (size_t i = 1; i < arr.size(); ++i) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return max;
}

int main() {
    std::vector<int> arr;

    // Ввод элементов массива с проверкой на корректность ввода
    int num = -1000;
    arr.push_back(5);
    arr.push_back(6);
    arr.push_back(7);

    // Поиск максимального значения
    int max = findMax(arr);
    std::cout << max << std::endl;

    return 0;
}

