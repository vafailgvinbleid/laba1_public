FROM ubuntu:latest

COPY ./lab1/releases/*.deb /tmp/

RUN dpkg-deb -R tmp/lab1.deb tmp/ 

ENTRYPOINT ["/tmp/usr/bin/findMax"]